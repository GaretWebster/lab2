package edu.ucsd.cs110w.temperature;
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	} 
	public String toString()
	{
		return "" + this.getValue() + " C";
	}
	@Override
	public Temperature toCelsius() {
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		Fahrenheit f = new Fahrenheit(((this.getValue()*9)/5)+32);
		return f;
	}
	public Temperature toKelvin() {
		// TODO: Complete this method
		Kelvin k = new Kelvin(this.getValue()-273);
		return k;
	}
}
